# Flinto Login

## Description

Sketch & Flinto login animation prototype.

![Sign In 1](signIn1.png)
![Sign In 2](signIn2.png)
![Success 1](success1.png)

## Technologies Used
- Sketch
- Flinto

![Gif of prototype](LoginGif.gif)
